package br.com.jlabs.project.business.project;

import java.io.Serializable;
import java.util.List;

import br.com.jlabs.project.entity.Company;
import br.com.jlabs.project.entity.Project;

public interface ProjectBusiness {

	List<Project> list(Company company);

	Project getProjectById(Serializable id);

	Project save(Project project);

}
