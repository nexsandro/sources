package br.com.jlabs.project.business.dir;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import br.com.jlabs.project.business.UserException;
import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.File;
import br.com.jlabs.project.entity.FileHistory;

public interface FileBusiness {
	
	Dir createDirectory(Serializable parentDir, String name) throws UserException;
	
	Map<String, List<Object>> listDir(Serializable dirId);

	File saveFile(File file);
	
//	File upload(Serializable dirId, String name, String originalFileName, InputStream input) throws FileNotFoundException, IOException;

	File upload(File file, InputStream input) throws IOException;
	
	Dir getParentDirById(Serializable id);

	Dir getDirById(Serializable id);

	Dir dirWithParents(Serializable dirId);

	void copyToStream(Serializable fileId, OutputStream outputStream) throws IOException;

	File getFileById(Serializable id);

	List<FileHistory> getHistory(File file);

	void copyToStream(Serializable fileId, Integer version,
			OutputStream outputStream) throws IOException;
	
}