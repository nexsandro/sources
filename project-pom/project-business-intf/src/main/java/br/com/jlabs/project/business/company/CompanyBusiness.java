package br.com.jlabs.project.business.company;

import java.io.Serializable;
import java.util.List;

import br.com.jlabs.project.business.UserException;
import br.com.jlabs.project.entity.Company;

public interface CompanyBusiness {

	List<Company> list();

	void save(Company company) throws UserException;

	Company getCompanyById(Serializable id);
	
}
