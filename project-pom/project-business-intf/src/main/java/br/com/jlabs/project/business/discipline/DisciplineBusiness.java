package br.com.jlabs.project.business.discipline;

import java.util.List;

import br.com.jlabs.project.entity.Discipline;

public interface DisciplineBusiness {

	List<Discipline> list();
	
	Discipline save(Discipline discipline);
	
	Discipline getDisciplineById(Integer id);
	
}
