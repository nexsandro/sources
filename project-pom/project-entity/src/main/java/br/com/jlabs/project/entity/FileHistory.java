package br.com.jlabs.project.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.jlabs.project.entity.enumeration.FileOperationEnum;
import br.com.jlabs.project.entity.enumeration.FileStatusEnum;

@javax.persistence.Entity
@Table(name="tb_file_hist")
public class FileHistory implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8974649240426335282L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="se_file_hist")
	@SequenceGenerator(name="se_file_hist", sequenceName="se_file_hist", allocationSize=1)
	@Column(name="sq_file_hist")
	private Long id;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sq_file", referencedColumnName="sq_file", nullable=false)
	private File file;
	
	@Column(name="no_user", nullable=false, length=255)
	private String user;

	@Column(name="dt_tmst", nullable=false)
	private Date timestamp;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sq_dir", nullable=false)
	private Dir dir;
	
	@Column(name="no_name", length=255, nullable=false)
	private String name;
	
	@Column(name="no_orig_name", length=255, nullable=false)
	private String originalName;
	
	@Column(name="nu_ip", length=50, nullable=false)
	private String ip;
	
	@Column(name="cd_situ", nullable=false)
	@Enumerated(EnumType.ORDINAL)
	private FileStatusEnum status;
	
	@Column(name="cd_oper", nullable=false)
	@Enumerated(EnumType.ORDINAL)
	private FileOperationEnum operation;
	
	@Column(name="nu_size", nullable=false)
	private Long size;
	
	@Column(name="nu_file_vers", nullable=false)
	private Integer fileVersion;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the dir
	 */
	public Dir getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Dir dir) {
		this.dir = dir;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the status
	 */
	public FileStatusEnum getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(FileStatusEnum status) {
		this.status = status;
	}

	/**
	 * @return the operation
	 */
	public FileOperationEnum getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(FileOperationEnum operation) {
		this.operation = operation;
	}

	/**
	 * @return the size
	 */
	public Long getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(Long size) {
		this.size = size;
	}

	/**
	 * @return the version
	 */
	public Integer getFileVersion() {
		return fileVersion;
	}

	/**
	 * @param version the version to set
	 */
	public void setFileVersion(Integer fileVersion) {
		this.fileVersion = fileVersion;
	}

	/**
	 * @return the originalName
	 */
	public String getOriginalName() {
		return originalName;
	}

	/**
	 * @param originalName the originalName to set
	 */
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

}
