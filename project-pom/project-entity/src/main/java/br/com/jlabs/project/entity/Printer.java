package br.com.jlabs.project.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="TB_PRNT")
public class Printer implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4056621290104612057L;

	@Id
	@Column(name="sq_prnt", length=11)
	@GeneratedValue(strategy=GenerationType.AUTO, generator="se_prnt")
	@SequenceGenerator(name="se_prnt", sequenceName="se_prnt", allocationSize=1, initialValue=1)
	private Long id;
	
	@Column(name="no_prnt", nullable=false)
	private String name;
	
	@OneToOne
	@JoinColumn(name="sq_cord")
	private User coordinator;

	/**
	 * Default constructor
	 */
	public Printer() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the coordinator
	 */
	public User getCoordinator() {
		return coordinator;
	}

	/**
	 * @param coordinator the coordinator to set
	 */
	public void setCoordinator(User coordinator) {
		this.coordinator = coordinator;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Printer other = (Printer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
