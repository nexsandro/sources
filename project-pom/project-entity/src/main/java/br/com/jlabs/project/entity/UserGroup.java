package br.com.jlabs.project.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@javax.persistence.Entity
@Table(name="TB_USER_GRUP")
public class UserGroup implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2537549086234832593L;

	/**
	 * Group id
	 */
	@Id
	@Column(name="sq_user_grup", length=11)
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SE_USER_GRUP")
	@SequenceGenerator(name="SE_USER_GRUP", sequenceName="SE_USER_GRUP", allocationSize=1, initialValue=1)
	private Long id;

	/**
	 * Group name
	 */
	@Column(name="no_user_grup", nullable=false)
	private String name;

	/**
	 * Version control
	 */
	@Version
	@Column(name="nu_vers")
	private Integer version;
	
	/**
	 * Users
	 */
	@ManyToMany(mappedBy="groups")
	private Set<User> users;
	
	/**
	 * Default constructor
	 */
	public UserGroup() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the users
	 */
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

}
