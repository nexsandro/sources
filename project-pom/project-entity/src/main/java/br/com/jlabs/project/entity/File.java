package br.com.jlabs.project.entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import br.com.jlabs.project.entity.enumeration.FileStatusEnum;

/**
 * Arquivo ou diret�rio.
 * 
 * @author sandro
 *
 */
@javax.persistence.Entity
@Table(name="tb_file")
public class File implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2353680323028145469L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="se_file")
	@SequenceGenerator(name="se_file", sequenceName="se_file", allocationSize=1, initialValue=1)
	@Column(name="sq_file", length=11)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sq_dir", nullable=false)
	private Dir dir;
	
	@Column(name="no_name", length=255, nullable=false)
	private String name;
	
	@Column(name="no_orig_name", length=255, nullable=false)
	private String originalName;
	
	@Column(name="cd_situ", nullable=false)
	@Enumerated(EnumType.ORDINAL)
	private FileStatusEnum status;

	@Column(name="tx_desc", length=1000)
	private String description;
	
	@Column(name="nu_size", nullable=false)
	private Long size;
	
	@Version
	@Column(name="nu_vers", nullable=false)
	private Integer version;
	
	transient
	private Integer level;

	/**
	 * Default constructor
	 */
	public File() {
		super();
	}	

	public File(Long id) {
		super();
		this.id = id;
	}

	public File(Dir dir, String name, FileStatusEnum status) {
		super();
		this.dir = dir;
		this.name = name;
		this.status = status;
		this.version = 1;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the dir
	 */
	public Dir getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Dir dir) {
		this.dir = dir;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the originalName
	 */
	public String getOriginalName() {
		return originalName;
	}

	/**
	 * @param originalName the originalName to set
	 */
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * @return the status
	 */
	public FileStatusEnum getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(FileStatusEnum status) {
		this.status = status;
	}

	/**
	 * @return the size
	 */
	public Long getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(Long size) {
		this.size = size;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		File other = (File) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
