package br.com.jlabs.project.entity.enumeration;

public enum RoleEnum {

	ADMIN,   // Ordinal 0, don't change order
	
	USER     // Ordinal 1, don't change order

}
