package br.com.jlabs.project.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="TB_PROJ")
public class Project implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6281756159485986011L;

	@Id
	@Column(name="sq_proj", length=11)
	@GeneratedValue(generator="SE_PROJ", strategy=GenerationType.AUTO)
	@SequenceGenerator(name="SE_PROJ", sequenceName="SE_PROJ", initialValue=1, allocationSize=1)
	private Long id;

	@Column(name="no_name", length=255, nullable=false, unique=true)
	private String name;
	
	
	@OneToMany()
	@JoinTable(name="tb_proj_user", 
		joinColumns=@JoinColumn(name="sq_proj", referencedColumnName="sq_proj"),
		inverseJoinColumns=@JoinColumn(name="sq_user", referencedColumnName="sq_user"))
	private Set<User> userMembers;

	@OneToMany()
	@JoinTable(name="tb_proj_disc",
			joinColumns=@JoinColumn(name="sq_proj", referencedColumnName="sq_proj"),
			inverseJoinColumns=@JoinColumn(name="sq_disc", referencedColumnName="sq_disc"))
	private Set<Discipline> disciplines;
	
	@ManyToOne
	@JoinColumn(name="sq_comp", referencedColumnName="sq_comp", nullable=false)
	private Company company;
	
	@OneToOne
	@JoinColumn(name="sq_dir", referencedColumnName="sq_dir", nullable=false)
	private Dir dir;
	
	/**
	 * Default constructor
	 */
	public Project() {
		super();
	}
	
	/**
	 * @return the id
	 */

	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the userMembers
	 */
	public Set<User> getUserMembers() {
		return userMembers;
	}

	/**
	 * @param userMembers the userMembers to set
	 */
	public void setUserMembers(Set<User> userMembers) {
		this.userMembers = userMembers;
	}

	/**
	 * @return the disciplines
	 */
	public Set<Discipline> getDisciplines() {
		return disciplines;
	}

	public void addDiscipline(Discipline discipline) {
		if (disciplines == null)
			disciplines = new HashSet<Discipline>();
		disciplines.add(discipline);
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the dir
	 */
	public Dir getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Dir dir) {
		this.dir = dir;
	}

	/**
	 * @param disciplines the disciplines to set
	 */
	public void setDisciplines(Set<Discipline> disciplines) {
		this.disciplines = disciplines;
	}

}
