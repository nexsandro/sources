package br.com.jlabs.project.entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.jlabs.project.entity.enumeration.ConfigParam;

@javax.persistence.Entity
@Table(name="tb_cnfg_parm")
public class ConfigParamEntity implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1301591832964274449L;

	@Id
	@Enumerated(EnumType.ORDINAL)
	@Column(name="cd_cnfg_parm", nullable=false, length=11)
	private ConfigParam configParam;
	
	@Column(name="no_valu", length=255)
	private String value;
	
	public ConfigParamEntity() {
		super();
	}

	/**
	 * @return the configParam
	 */
	public ConfigParam getConfigParam() {
		return configParam;
	}

	/**
	 * @param configParam the configParam to set
	 */
	public void setConfigParam(ConfigParam configParam) {
		this.configParam = configParam;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
