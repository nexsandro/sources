package br.com.jlabs.project.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@javax.persistence.Entity
@Table(name="TB_COMP")
public class Company implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3482659694515211890L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SE_COMP")
	@SequenceGenerator(name="SE_COMP", sequenceName="SE_COMP", allocationSize=1, initialValue=1)
	@Column(name="sq_comp",  length=11)
	private Long id;
	
	@Column(name="no_name", length=400)
	private String name;
	
	@Column(name="no_cnpj", length=14)
	private String cnpj;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sq_addf")
	private Address address;

	@OneToMany(mappedBy="company")
	private Set<Project> projects;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sq_dir", referencedColumnName="sq_dir")
	private Dir dir;
	
	@Version
	@Column(name="nu_vers")
	private Integer version;
	
	public Company() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nameString
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param nameString the nameString to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * @param cnpj the cnpj to set
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the projects
	 */
	public Set<Project> getProjects() {
		return projects;
	}

	/**
	 * @param projects the projects to set
	 */
	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public void addProject(Project project) {
		if (projects == null)
			projects = new HashSet<Project>();
		projects.add(project);
	}

	/**
	 * @return the dir
	 */
	public Dir getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Dir dir) {
		this.dir = dir;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(name.length() + 50);
		builder.append("Company: id=").append(id).append(", name='").append(name).append("'");
		return builder.toString();
	}
}
