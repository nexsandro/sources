/**
 * 
 */
package br.com.jlabs.project.entity.enumeration;

/**
 * @author sandro
 *
 */
public enum FileOperationEnum {

	REGISTER,
	
	UPDATE,
	
	DELETE,
	
	DEACTIVATE
	
}
