package br.com.jlabs.project.entity.enumeration;

/**
 * Status dos arquivos
 * 
 * @author sandro
 *
 */
public enum FileStatusEnum {
	
	ACTIVE,
	
	STALE,
	
	HIDDEN
	
}
