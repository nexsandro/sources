package br.com.jlabs.project.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@javax.persistence.Entity
@Table(name="TB_DISC")
public class Discipline implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4019058693154457709L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SE_DISC")
	@SequenceGenerator(name="SE_DISC", sequenceName="SE_DISC", allocationSize=1, initialValue=1)
	@Column(name="sq_disc", length=11)
	private Integer id;
	
	@Column(name="no_name", length=255, nullable=false)
	private String name;
	
	@Version
	@Column(name="nu_vers", nullable=false)
	private Integer version;
	
	public Discipline() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

}
