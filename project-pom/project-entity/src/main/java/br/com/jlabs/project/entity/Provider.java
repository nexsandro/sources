package br.com.jlabs.project.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Represents a provider.
 * 
 * @author sandro
 *
 */
@javax.persistence.Entity
@Table(name="tb_prov")
public class Provider implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5094575394661473058L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="se_prov")
	@SequenceGenerator(name="se_prov", sequenceName="se_prov", allocationSize=1, initialValue=1)
	@Column(name="sq_prov")
	private Integer id;
	
	@Column(name="no_name", nullable=false, length=255)
	private String name;
	
	@Column(name="no_short_name", nullable=false, length=30)
	private String shortName;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="tb_prov_proj",
			joinColumns=@JoinColumn(name="sq_prov", referencedColumnName="sq_prov"),
			inverseJoinColumns=@JoinColumn(name="sq_proj", referencedColumnName="sq_proj"))
	private Set<Project> projects;
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="tb_prov_prnt",
			joinColumns=@JoinColumn(name="sq_prov", referencedColumnName="sq_prov"),
			inverseJoinColumns=@JoinColumn(name="sq_prnt", referencedColumnName="sq_prnt"))
	private Set<Printer> printers;
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="tb_prov_user",
			joinColumns=@JoinColumn(name="sq_prov", referencedColumnName="sq_prov"),
			inverseJoinColumns=@JoinColumn(name="sq_user", referencedColumnName="sq_user"))
	private Set<User> users;
	
	/**
	 * Default constructor
	 */
	public Provider() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the projects
	 */
	public Set<Project> getProjects() {
		return projects;
	}

	/**
	 * @param projects the projects to set
	 */
	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	/**
	 * @return the printers
	 */
	public Set<Printer> getPrinters() {
		return printers;
	}

	/**
	 * @param printers the printers to set
	 */
	public void setPrinters(Set<Printer> printers) {
		this.printers = printers;
	}

	/**
	 * @return the users
	 */
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
}
