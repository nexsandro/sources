package br.com.jlabs.project.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@javax.persistence.Entity
@Table(name="tb_dir")
public class Dir implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2202413517625241056L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SE_DIR")
	@SequenceGenerator(name="SE_DIR", sequenceName="SE_DIR", allocationSize=1, initialValue=1)
	@Column(name="sq_dir", nullable=false, length=11)
	private Long id;
	
	@Column(name="no_dir", length=255, nullable=false)
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="dir")
	private Set<File> files;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sq_prnt", referencedColumnName="sq_dir")
	private Dir parent;
//	
//	@Column(name="no_path", nullable=false)
//	private String path;
	
	@Version
	@Column(name="nu_vers", nullable=false)
	private Integer version;
	
	
	public Dir() {
		super();
	}

	
	public Dir(Long id) {
		super();
		this.id = id;
	}


	public Dir(Dir parent, String name) {
		super();
		this.name = name;
		this.parent = parent;
		
//		if (parent != null)
//			path = parent.getPath() + "/" + name;
//		else
//			path = "/" + name;
		this.version = 1;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the files
	 */
	public Set<File> getFiles() {
		return files;
	}

	/**
	 * @param files the files to set
	 */
	public void setFiles(Set<File> files) {
		this.files = files;
	}

//	/**
//	 * @return the path
//	 */
//	public String getPath() {
//		return path;
//	}
//
//	/**
//	 * @param path the path to set
//	 */
//	public void setPath(String path) {
//		this.path = path;
//	}

	/**
	 * @return the parent
	 */
	public Dir getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(Dir parent) {
		this.parent = parent;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dir other = (Dir) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Dir: id=" + id + ",  name=" + name;
	}
}
