package br.com.jlabs.project.entity;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.jlabs.project.entity.enumeration.RoleEnum;

@javax.persistence.Entity
@Table(name="TB_USER")
public class User implements Entity {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8462381506247270618L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SE_USER")
	@SequenceGenerator(name="SE_USER", sequenceName="SE_USER", initialValue=1, allocationSize=1)
	@Column(name="sq_user", length=11)
	private Long id;
	
	@Column(name="no_logn")
	private String login;
	
	@Column(name="no_pass")
	private String password;
	
	@Column(name="no_name")
	private String name;
	
	@Column(name="no_mail")
	private String email;

	@Column(name="no_phon")
	private String phone;
	
	@OneToOne
	@JoinColumn(name="sq_comp", nullable=false)
	private Company company;

	@ElementCollection(fetch=FetchType.LAZY)
	@Enumerated(EnumType.ORDINAL)
	@CollectionTable(name="tb_user_role",
			joinColumns=@JoinColumn(name="sq_user", referencedColumnName="sq_user"))
	@Column(name="sq_role_id")
	private Set<RoleEnum> roles;
	
	@Column(name="nu_vers")
	private String version;
	
	@ManyToMany
	@JoinTable(name="tb_user_grup_user",
		joinColumns=@JoinColumn(name="sq_user", referencedColumnName="sq_user"),
		inverseJoinColumns=@JoinColumn(name="sq_user_grup", referencedColumnName="sq_user_grup"))
	private Set<UserGroup> groups;
	
	/**
	 * Default constructor
	 */
	public User() {
		super();
	}
	
	/**
	 * Constructor with fields
	 * @param logon
	 * @param password
	 */
	public User(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the logon
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param logon the logon to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the groups
	 */
	public Set<UserGroup> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(Set<UserGroup> groups) {
		this.groups = groups;
	}

	/**
	 * @return the roles
	 */
	public Set<RoleEnum> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Set<RoleEnum> roles) {
		this.roles = roles;
	}
	
}
