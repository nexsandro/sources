package br.com.jlabs.project.business.company;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.jlabs.project.business.UserException;
import br.com.jlabs.project.business.dir.FileBusiness;
import br.com.jlabs.project.dao.company.CompanyDao;
import br.com.jlabs.project.entity.Company;
import br.com.jlabs.project.entity.Dir;

@Service("companyBusiness")
public class CompanyBusinessImpl implements CompanyBusiness {

	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private FileBusiness fileBusiness;
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	public List<Company> list() {

		return companyDao.findAll(new String[] {"dir"});
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(Company company) throws UserException {
		
		// If creating company, create it's directory too
		if (company.getId() == null) {
			Dir dir = fileBusiness.createDirectory(null, company.getName());
			company.setDir(dir);
		}
		
		companyDao.save(company);
	}

	/**
	 * @param companyDao the companyDao to set
	 */
	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public Company getCompanyById(Serializable id) {
		return companyDao.findOne("id", id, new String[] {"address"});
	}

	/**
	 * @param fileBusiness the fileBusiness to set
	 */
	public void setFileBusiness(FileBusiness fileBusiness) {
		this.fileBusiness = fileBusiness;
	}
	
}
