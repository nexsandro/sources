package br.com.jlabs.project.business.discipline;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.jlabs.project.dao.discipline.DisciplineDao;
import br.com.jlabs.project.entity.Discipline;

@Service("disciplineBusiness")
public class DisciplineBusinessImpl implements DisciplineBusiness {

	@Autowired
	private DisciplineDao disciplineDao;
	
	@Transactional(propagation=Propagation.REQUIRED)
	public List<Discipline> list() {
		return disciplineDao.findAll();
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public Discipline save(Discipline discipline) {
		return disciplineDao.save(discipline);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public Discipline getDisciplineById(Integer id) {
		return disciplineDao.getDisciplineById(id);
	}

	/**
	 * @param disciplineDao the disciplineDao to set
	 */
	public void setDisciplineDao(DisciplineDao disciplineDao) {
		this.disciplineDao = disciplineDao;
	}

}
