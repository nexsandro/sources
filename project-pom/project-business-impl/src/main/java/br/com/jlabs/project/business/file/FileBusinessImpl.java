package br.com.jlabs.project.business.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.jlabs.common.ExpectedFailureException;
import br.com.jlabs.infra.context.BusinessContext;
import br.com.jlabs.project.business.UserException;
import br.com.jlabs.project.business.dir.FileBusiness;
import br.com.jlabs.project.dao.config.ConfigDao;
import br.com.jlabs.project.dao.file.FileDao;
import br.com.jlabs.project.entity.ConfigParamEntity;
import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.File;
import br.com.jlabs.project.entity.FileHistory;
import br.com.jlabs.project.entity.enumeration.ConfigParam;
import br.com.jlabs.project.entity.enumeration.FileOperationEnum;
import br.com.jlabs.project.entity.enumeration.FileStatusEnum;

@Service("fileBusiness")
public class FileBusinessImpl implements FileBusiness {

	@Autowired
	private FileDao fileDao;

	@Autowired
	private ConfigDao configDao;
	
	@Autowired
	private BusinessContext businessContext;
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Dir createDirectory(Serializable parentDirId, String name) throws UserException {
		
		Dir parentDir = null;
		ConfigParamEntity filesystemRoot = configDao.getParam(ConfigParam.ROOT_DIR);
		
		//  If parent == null, then root directory is the parent
		if (parentDirId != null) {
			parentDir = fileDao.getDirById(parentDirId);
		}
		
		Dir dir = fileDao.createDir(parentDir, name);

		// Create physical directory if not exist
		try {
			
			StringBuilder path = recursiveUpToRoot(dir);
			
			Files.createDirectories(Paths.get(filesystemRoot.getValue() + path.toString()));
			
		} catch (IOException e) {
			throw new CreateDirException(name);
		}
		
		return dir;

	}

	private StringBuilder recursiveUpToRoot(Dir dir) {
		// Create Path to file
		Dir dirWithParents = fileDao.dirWithParents(dir.getId());
		StringBuilder path = new StringBuilder();
		while(dirWithParents != null) {
			path.insert(0, dirWithParents.getId()).insert(0, "/");
			dirWithParents = dirWithParents.getParent();
		}
		return path;
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public File saveFile(File file) {
		return fileDao.saveFile(file);
	}
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Map<String, List<Object>> listDir(Serializable dirId) {
		Dir dir = fileDao.getDirById(dirId);
		return fileDao.listDir(dir);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public File upload(Serializable dirId, String name, String originalFileName, InputStream input) throws FileNotFoundException, IOException {

		Dir dir = fileDao.getDirById(dirId);
		File file = new File(dir, name, FileStatusEnum.ACTIVE);
		fileDao.saveFile(file);
		
		ConfigParamEntity filesystemRoot = configDao.getParam(ConfigParam.ROOT_DIR);

		// Create Path to file
		StringBuilder path = recursiveUpToRoot(dir);

		// Copy file to destiny file
		Files.copy(input, Paths.get(filesystemRoot.getValue() + path.toString() + "/" + composeFileName(file, file.getVersion())));
		
		return file;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public File getFileById(Serializable id) {
		return fileDao.getFileById(id);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public void copyToStream(Serializable fileId, OutputStream outputStream) throws IOException {
		copyToStream(fileId, outputStream);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public void copyToStream(Serializable fileId, Integer version, OutputStream outputStream) throws IOException {
		
		ConfigParamEntity filesystemRoot = configDao.getParam(ConfigParam.ROOT_DIR);

		File file;
		file = fileDao.getFileById(fileId);
		
		// If is file history
		if (version != null) {
			version = file.getVersion();
		}

		// Create Path to file
		StringBuilder path = recursiveUpToRoot(file.getDir());

		// Copy file to destiny file
		Files.copy(Paths.get(filesystemRoot.getValue() + path.toString() + "/" + composeFileName(file, version)), outputStream);
		
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Dir getDirById(Serializable id) {
		return fileDao.getDirById(id);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public Dir getParentDirById(Serializable childId) {
		return fileDao.getParentDirById(childId);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public Dir dirWithParents(Serializable dirId) {
		return fileDao.dirWithParents(dirId);
	}
	
	/**
	 * @param dirDao the dirDao to set
	 */
	public void setDirDao(FileDao dirDao) {
		this.fileDao = dirDao;
	}
	
	/**
	 * @param configDao the configDao to set
	 */
	public void setConfigDao(ConfigDao configDao) {
		this.configDao = configDao;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public List<FileHistory> getHistory(File file) {

		return fileDao.getHistory(file);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public File upload(File file, InputStream input) throws IOException {
		
		File tempFile = file;
		if (file.getId() != null) {
			file = fileDao.findOne(tempFile.getId());
			file.setName(tempFile.getName());
			file.setDescription(tempFile.getDescription());
			file.setVersion(tempFile.getVersion());
		}
		
		// Lookup for existing name
		if (fileDao.existFileNameInDir(file.getName(), file.getDir().getId()))
			throw new ExpectedFailureException("exception.file.upload.existing.file");
		
		Dir dir = fileDao.getDirById(file.getDir().getId());

		
		// Register file
		file.setDir(dir);
		file.setStatus(FileStatusEnum.ACTIVE);
		fileDao.saveFile(file);
		
		// Register file history
		FileHistory fileHistory = new FileHistory();
		fileHistory.setFile(file);
		fileHistory.setDir(file.getDir());
		fileHistory.setName(file.getName());
		fileHistory.setOperation(FileOperationEnum.UPDATE);
		fileHistory.setOriginalName(file.getOriginalName());
		fileHistory.setStatus(file.getStatus());
		fileHistory.setTimestamp(new Date());
		fileHistory.setSize(file.getSize());
		fileHistory.setFileVersion(file.getVersion());
		fileHistory.setUser(businessContext.getUserName());
		fileHistory.setIp(businessContext.getUserIp());
		fileDao.saveHistory(fileHistory);
		
		// Create Path to file
		ConfigParamEntity filesystemRoot = configDao.getParam(ConfigParam.ROOT_DIR);
		StringBuilder path = recursiveUpToRoot(dir);

		// Copy file to destiny file
		Files.copy(input, Paths.get(filesystemRoot.getValue() + path.toString() + "/" + composeFileName(file, file.getVersion())));
		
		return file;
	}

	/**
	 * Compose directory file name.
	 * @param file
	 * @return
	 */
	private String composeFileName(File file, Integer version) {
		return file.getId() + "-" + version;
	}

}
