package br.com.jlabs.project.business.project;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.jlabs.project.business.dir.FileBusiness;
import br.com.jlabs.project.dao.company.CompanyDao;
import br.com.jlabs.project.dao.project.ProjectDao;
import br.com.jlabs.project.entity.Company;
import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.Project;

@Service("projectBusiness")
public class ProjectBusinessImpl implements ProjectBusiness {

	@Autowired
	ProjectDao projectDao;
	
	@Autowired
	FileBusiness fileBusiness;
	
	@Autowired
	CompanyDao companyDao;
	
	@Transactional(propagation=Propagation.REQUIRED)	
	public List<Project> list(Company company) {
		return projectDao.list(company);
	}

	@Transactional(propagation=Propagation.REQUIRED)	
	public Project getProjectById(Serializable id) {
		return projectDao.getProjectById(id);
	}

	@Transactional(propagation=Propagation.REQUIRED)	
	public Project save(Project project) {
		
		if (project.getId() == null) {
			Company company = companyDao.findOne(project.getCompany().getId());
			Dir projDirectory = fileBusiness.createDirectory(company.getDir().getId(), project.getName());
			project.setDir(projDirectory);
			project.setCompany(company);
		}
		
		return projectDao.save(project);
	}
	
	/**
	 * @param projectDao the projectDao to set
	 */
	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}
	
}
