package br.com.jlabs.project.business.file;

import br.com.jlabs.project.business.UserException;

public class CreateDirException extends UserException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4023486973083848381L;

	private String dirName;

	public CreateDirException(String dirName) {
		super();
		this.dirName = dirName;
	}

}
