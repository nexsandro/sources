package br.com.jlabs.common;

/**
 * 
 * An well known exception.
 * 
 * @author sebastiao.santos
 *
 */
public class ExpectedFailureException extends RuntimeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2287582499703239515L;

	/**
	 * Key to search message
	 */
	private String msgKey;
	
	/**
	 * Parameters to message
	 */
	private Object[] msgParams;

	/**
	 * An parameterized exception
	 * 
	 * @param msgKey
	 * @param msgParams
	 */
	public ExpectedFailureException(String msgKey, Object[] msgParams) {
		super();
		this.msgKey = msgKey;
		this.msgParams = msgParams;
	}

	/**
	 * An exception with message only
	 * @param msgKey
	 */
	public ExpectedFailureException(String msgKey) {
		super();
		this.msgKey = msgKey;
	}

	/**
	 * @return the msgKey
	 */
	public String getMsgKey() {
		return msgKey;
	}

	/**
	 * @return the msgParams
	 */
	public Object[] getMsgParams() {
		return msgParams;
	}

}
