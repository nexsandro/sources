package br.com.jlabs.project.web.control;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jlabs.project.business.UserException;
import br.com.jlabs.project.business.dir.FileBusiness;
import br.com.jlabs.project.entity.Dir;

@Controller
@RequestMapping("/sec/dir")
public class DirController {
	
	@Autowired
	private FileBusiness fileBusiness;
	
	
	@RequestMapping(value="/navigate")
	public ModelAndView navigate(Long dirId, Writer writer) throws IOException {

		// Search for dir content
		Dir dir = fileBusiness.dirWithParents(dirId);
		ModelAndView result = new ModelAndView("/dir/navigate");

		// Prepare breadcrumbs
		LinkedList<Dir> breadcrumb = new LinkedList<Dir>();
		Dir tmpDir = dir;
		while (tmpDir != null) {
			breadcrumb.push(tmpDir);
			tmpDir = tmpDir.getParent();
		}

		// Populate result object
		result.addObject("dirContent", fileBusiness.listDir(dirId));
		result.addObject("dir", dir);
		result.addObject("breadcrumb", breadcrumb);

		return result;
	}
	
	
	@RequestMapping(value="/edit")
	public ModelAndView edit(@ModelAttribute Dir dir, BindingResult bindings) {
		ModelAndView result = new ModelAndView("/dir/edit");

		// edit
		if (dir.getId() != null) {
			dir = fileBusiness.getDirById(dir.getId());
		}
		dir.setParent(fileBusiness.getDirById(dir.getParent().getId()));

		result.addObject("dir", dir);
		
		return result;
	}
	
	@RequestMapping(value="/save")
	public ModelAndView save(@ModelAttribute Dir dir) throws UserException {
		ModelAndView result = new ModelAndView("redirect:navigate");
		Dir newDir = dir;

		// edit
		if (dir.getId() != null) {
			newDir = fileBusiness.getDirById(dir.getId());
			newDir.setName(dir.getName());
			newDir.setParent(fileBusiness.getDirById(dir.getParent().getId()));
		}
		
		dir = fileBusiness.createDirectory(newDir.getParent().getId(), newDir.getName());

		result.addObject("dirId", dir.getParent().getId());
		
		return result;
	}
	
	@RequestMapping(value="/create")
	public ModelAndView createDir(Long parentDir, String dir) throws UserException {
		fileBusiness.createDirectory(parentDir, dir);
		return new ModelAndView("redirect:/dir/list");
	}
	
	public void setFileBusiness(FileBusiness fileBusiness) {
		this.fileBusiness = fileBusiness;
	}
}
