package br.com.jlabs.project.web.control.file;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.File;
import br.com.jlabs.project.entity.FileHistory;
import br.com.jlabs.project.web.Model;

/**
 * The upload model.
 * 
 * @author sandro
 *
 */
public class FileEditModel implements Model {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3740722453876659143L;

	private MultipartFile multipartFile;
	
	private Dir dir;
	
	private File file;
	
	private List<FileHistory> listFileHistory;
	
	public FileEditModel() {
		super();
	}
	
	public FileEditModel(File file, List<FileHistory> listFileHistory) {
		super();
		this.file = file;
		this.listFileHistory = listFileHistory;
	}

	/**
	 * @return the multipartFile
	 */
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	/**
	 * @param multipartFile the multipartFile to set
	 */
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	/**
	 * @return the dir
	 */
	public Dir getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Dir dir) {
		this.dir = dir;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the listFileHistory
	 */
	public List<FileHistory> getListFileHistory() {
		return listFileHistory;
	}

	/**
	 * @param listFileHistory the listFileHistory to set
	 */
	public void setListFileHistory(List<FileHistory> listFileHistory) {
		this.listFileHistory = listFileHistory;
	}

}
