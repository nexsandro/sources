package br.com.jlabs.project.web.control.file;

import java.io.Serializable;

public class FileUploadResponse implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3265812698958230342L;

	/**
	 * True if file name invalid
	 */
	private boolean fileNameInvalid;
	
	/**
	 * True if file content empty
	 */
	private boolean fileContentEmpty;

	/**
	 * Message to the user
	 */
	private String userMessage;
	
	/**
	 * True if upload success
	 */
	private boolean success;
	
	/**
	 * @return the fileNameInvalid
	 */
	public boolean isFileNameInvalid() {
		return fileNameInvalid;
	}

	/**
	 * @param fileNameInvalid the fileNameInvalid to set
	 */
	public void setFileNameInvalid(boolean fileNameInvalid) {
		this.fileNameInvalid = fileNameInvalid;
	}

	/**
	 * @return the userMessage
	 */
	public String getUserMessage() {
		return userMessage;
	}

	/**
	 * @param userMessage the userMessage to set
	 */
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	/**
	 * @return the fileContentEmpty
	 */
	public boolean isFileContentEmpty() {
		return fileContentEmpty;
	}

	/**
	 * @param fileContentEmpty the fileContentEmpty to set
	 */
	public void setFileContentEmpty(boolean fileContentEmpty) {
		this.fileContentEmpty = fileContentEmpty;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
}
