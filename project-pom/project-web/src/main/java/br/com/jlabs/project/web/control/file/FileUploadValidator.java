package br.com.jlabs.project.web.control.file;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class FileUploadValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return false;
	}

	public void validate(Object target, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "file.name", "file.name.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "file", "file.required");
		
		FileEditModel model = (FileEditModel) target;
		
		if(model.getMultipartFile().isEmpty())
			errors.rejectValue("multipartFile", "file.multipartFile.required");
	}

}
