package br.com.jlabs.project.web.control.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jlabs.project.business.dir.FileBusiness;

@Controller
@RequestMapping(value="/sec")
public class FileHistoryController {
	
	@Autowired
	FileBusiness fileBusiness;

	@RequestMapping(value="/file/history")
	public ModelAndView edit(@ModelAttribute FileEditModel model) {

		model.setFile(fileBusiness.getFileById(model.getFile().getId()));
		model.setListFileHistory(fileBusiness.getHistory(model.getFile()));
		
		return new ModelAndView("/file/history", "form", model);
	}
	
}
