package br.com.jlabs.project.web.control.file.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.jlabs.project.web.control.file.FileEditModel;

public class UploadValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return FileEditModel.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		
		ValidationUtils.rejectIfEmpty(errors, "multipartFile.empty", "field.required");
		ValidationUtils.rejectIfEmpty(errors, "name", "field.required");

	}

}
