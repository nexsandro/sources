package br.com.jlabs.project.web.control.file;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.File;

/**
 * File upload view Model
 * 
 * @author sandro
 *
 */
public class FileUploadRequest implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 536909320205638272L;

	/**
	 * File to upload
	 */
	private File file;

	/**
	 * Directory to add file
	 */
	private Dir dir;
	
	/**
	 * Multipart upload parts
	 */
	private MultipartFile multipartFile;

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the dir
	 */
	public Dir getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Dir dir) {
		this.dir = dir;
	}

	/**
	 * @return the multipartFile
	 */
	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	/**
	 * @param multipartFile the multipartFile to set
	 */
	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	@Override
	public String toString() {
		return "FileUploadModel: file.dir.id=" + file.getDir().getId() + ", file.idd=" + file.getId() + ", fileName=" + file.getName() + ", multipartFile=" + multipartFile;
	}
}
