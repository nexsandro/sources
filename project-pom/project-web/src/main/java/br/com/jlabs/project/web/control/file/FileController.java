package br.com.jlabs.project.web.control.file;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.jlabs.common.ExpectedFailureException;
import br.com.jlabs.project.business.dir.FileBusiness;
import br.com.jlabs.project.entity.File;

@Controller
@RequestMapping(value="/sec")
public class FileController {

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	FileBusiness fileBusiness;
    
	@RequestMapping(value="/file/input")
	public ModelAndView input(@ModelAttribute FileEditModel model) {
		
		ModelAndView result = new ModelAndView("/file/input", "form", model);
		
		result.addObject("dir", fileBusiness.getDirById(model.getDir().getId()));
		result.addObject("file", model.getFile());
		
		return result;
	}
	
    @RequestMapping(value = "/file/upload", method = RequestMethod.POST)
    public ModelAndView upload(@ModelAttribute FileEditModel model, BindingResult bindingResult) throws IOException {

    	// Validate here
    	ModelAndView result = new ModelAndView();
    	FileUploadValidator validator = new FileUploadValidator();
    	validator.validate(model, bindingResult);
    	
    	if (bindingResult.hasErrors())
    		return input(model);
    	
    	// If validation passed
    	model.getFile().setDir(model.getDir());
    	model.getFile().setOriginalName(model.getMultipartFile().getOriginalFilename());
    	model.getFile().setSize(model.getMultipartFile().getSize());
    	model.getFile().setVersion(model.getFile().getVersion());
    	fileBusiness.upload(model.getFile(), model.getMultipartFile().getInputStream());
    	result.setViewName("redirect:/sec/dir/navigate?dirId=" + model.getFile().getDir().getId());

        return result;
    }
	
    @RequestMapping(value = "/file/ajax/upload", method = RequestMethod.POST)
    public @ResponseBody FileUploadResponse ajaxUpload(@ModelAttribute FileUploadRequest model) throws IOException {

    	// Validate here
    	boolean modelValid = true;
    	FileUploadResponse response = new FileUploadResponse(); 
    	
    	response.setSuccess(false);
    	if (model.getFile().getName().trim().length() == 0) { 
    		modelValid = false;
    		response.setFileNameInvalid(true);
    	}

    	if (model.getMultipartFile().isEmpty()) {
    		modelValid = false;
    		response.setFileContentEmpty(true);
    	}
    	
    	if (!modelValid) return response;
    		
    	
    	// If validation passed
    	model.getFile().setDir(model.getDir());
    	model.getFile().setOriginalName(model.getMultipartFile().getOriginalFilename());
    	model.getFile().setSize(model.getMultipartFile().getSize());
    	
    	try {
    		fileBusiness.upload(model.getFile(), model.getMultipartFile().getInputStream());
    		response.setUserMessage(messageSource.getMessage("file.upload.success", null, Locale.getDefault()));
        	response.setSuccess(true);
    	} catch (ExpectedFailureException e) {
    		response.setUserMessage(messageSource.getMessage(e.getMsgKey(), e.getMsgParams(), Locale.getDefault()));
    	}

        return response;
    }

    
	@RequestMapping(value = "/file/id/{fileId}/version/{version}", method = RequestMethod.GET)
    public void download(@PathVariable Long fileId, @PathVariable Integer version, HttpServletResponse response) {
		try {

			File file = fileBusiness.getFileById(fileId);
			
			// copy it to response's OutputStream
			response.addHeader("Content-Disposition", "attachment; filename=" + file.getName());
			fileBusiness.copyToStream(fileId, version, response.getOutputStream());
			response.flushBuffer();
			
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}

    }
	
	/**
	 * @param fileBusiness the fileBusiness to set
	 */
	public void setFileBusiness(FileBusiness fileBusiness) {
		this.fileBusiness = fileBusiness;
	}
	
}
