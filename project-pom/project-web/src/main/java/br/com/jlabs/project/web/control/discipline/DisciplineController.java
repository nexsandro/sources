package br.com.jlabs.project.web.control.discipline;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jlabs.project.business.discipline.DisciplineBusiness;
import br.com.jlabs.project.entity.Discipline;

@Controller
@RequestMapping("/sec/discipline")
public class DisciplineController {

	@Autowired
	private DisciplineBusiness disciplineBusiness;
	
	@RequestMapping("/list")
	public ModelAndView list() {
		List<Discipline> disciplines = disciplineBusiness.list();
		ModelAndView modelAndView = new ModelAndView("/discipline/list");
		modelAndView.addObject("disciplines", disciplines);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Discipline discipline, BindingResult companyBindingResult) {
		
		if (discipline.getId() != null)
			discipline = disciplineBusiness.getDisciplineById(discipline.getId());
		
		ModelAndView modelAndView = new ModelAndView("/discipline/edit");
		modelAndView.addObject("discipline", discipline);
		
		return modelAndView;
	}	
	
	@RequestMapping("/save")
	public ModelAndView save(Discipline discipline, BindingResult companyBindingResult) {
		
		disciplineBusiness.save(discipline);
		
		return new ModelAndView("redirect:list");
	}	
}
