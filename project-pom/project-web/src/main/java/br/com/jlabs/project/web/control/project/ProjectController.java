package br.com.jlabs.project.web.control.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jlabs.project.business.UserException;
import br.com.jlabs.project.business.project.ProjectBusiness;
import br.com.jlabs.project.entity.Company;
import br.com.jlabs.project.entity.Project;

@Controller
@RequestMapping("/sec/project")
public class ProjectController {

	/**
	 * Project business
	 */
	private ProjectBusiness projectBusiness;

	@Autowired
	public ProjectController(ProjectBusiness projectBusiness) {
		super();
		this.projectBusiness = projectBusiness;
	}

	
	@RequestMapping("/list")
	public ModelAndView list(Company company) {
		
		List<Project> projects = projectBusiness.list(company);
		ModelAndView modelAndView = new ModelAndView("/project/list");
		modelAndView.addObject("projects", projects);
		modelAndView.addObject("company", company);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(Project project) {
		
		if (project.getId() != null)
			project = projectBusiness.getProjectById(project.getId());
		
		ModelAndView modelAndView = new ModelAndView("/project/edit");
		modelAndView.addObject("project", project);
		
		return modelAndView;
	}
	
	@RequestMapping("/save")
	public ModelAndView save(Project project, BindingResult companyBindingResult) throws UserException {

		projectBusiness.save(project);
		
		return new ModelAndView("redirect:list?id=" + project.getCompany().getId());
	}

}
