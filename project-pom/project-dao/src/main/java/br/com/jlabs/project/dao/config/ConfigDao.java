package br.com.jlabs.project.dao.config;

import br.com.jlabs.project.entity.ConfigParamEntity;
import br.com.jlabs.project.entity.enumeration.ConfigParam;

public interface ConfigDao {

	ConfigParamEntity getParam(ConfigParam param);
	
}
