package br.com.jlabs.project.dao.hibernate.file;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.jlabs.project.dao.AbstractHibernateDao;
import br.com.jlabs.project.dao.file.FileDao;
import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.File;
import br.com.jlabs.project.entity.FileHistory;

@Component("fileDao")
public class FileDaoImpl extends AbstractHibernateDao<File> implements FileDao {

	public FileDaoImpl() {
		super(File.class);
	}
	
	public boolean existFileNameInDir(String name, Long dirId) {
		
		Session session = sessionFactory.getCurrentSession();

		// Retrieve files in the dir
		Query query = session.createQuery("select count(*) from " + File.class.getCanonicalName() + " file where UPPER(name) = :name "
				+ " and file.dir.id = :dirId");
		query.setString("name", name.toUpperCase());
		query.setLong("dirId", dirId);
		
		return ((Number) query.uniqueResult()).intValue() != 0;
		
	}

	
	public Dir createDir(Dir parent, String name) {
		Session session;
		
		try {
			session = sessionFactory.getCurrentSession();
			Dir dir = new Dir(parent, name);
			session.save(dir);
			session.flush();
			session.evict(dir);
			
			return dir;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Dir getDirById(Serializable id) {

		Session session = sessionFactory.getCurrentSession();
		Dir dir = (Dir) session.get(Dir.class, id);
		session.evict(dir);
		
		return dir;
	}

	public Dir saveDir(Dir dir) {
		Session session = sessionFactory.getCurrentSession();
		session.save(dir);
		return dir;
	}
	
	public Map<String, List<Object>> listDir(Dir dir) {
		
		Map<String, List<Object>> items = new HashMap<String, List<Object>>(); 
		
		Session session = sessionFactory.getCurrentSession();

		// Retrieve files in the dir
		Criteria criteriaDir = session.createCriteria(Dir.class);
		
		items.put("dirs", 
				criteriaDir
				.add(Restrictions.eq("parent.id", dir.getId()))
				.list());
		
		// Retrieve dirs in the dir
		Criteria criteriaFile = session.createCriteria(File.class);
		items.put("files", criteriaFile.add(Restrictions.eq("dir.id", dir.getId())).list());
		
		return items;
	}

	public File saveFile(File file) {
		
		Session session = sessionFactory.getCurrentSession();
		session.save(file);
		
		return file;
	}
	
	public Dir dirWithParents(Serializable dirId) {
		
		Dir dir = getDirById(dirId);
		Dir tmpDir = dir;
		while (tmpDir.getParent() != null) {
			Dir parent = getDirById(tmpDir.getParent().getId());
			tmpDir.setParent(parent);
			tmpDir = parent;
		}
		
		return dir;
	}

	public Dir getParentDirById(Serializable childId) {
		Session session = sessionFactory.getCurrentSession();
		return (Dir) 
				session.createCriteria(Dir.class)
			.setFetchMode("child", FetchMode.JOIN)
			.add(Restrictions.eq("child.id", childId))
			.uniqueResult();
	}

	public File getFileById(Serializable id) {
		Session session = sessionFactory.getCurrentSession();
		File file = (File) session.get(File.class, id);
		session.evict(file);
		
		return file;
	}

	public List<FileHistory> getHistory(File file) {
		Session session = sessionFactory.getCurrentSession();
		
		Criteria criteria = session.createCriteria(FileHistory.class)
			.add(Restrictions.eq("file", file))
			.addOrder(Order.desc("timestamp"));
		
		return (List<FileHistory>) criteria.list();
	}

	public FileHistory saveHistory(FileHistory fileHistory) {
		Session session = sessionFactory.getCurrentSession();
		session.save(fileHistory);
		
		return fileHistory;
	}

}
