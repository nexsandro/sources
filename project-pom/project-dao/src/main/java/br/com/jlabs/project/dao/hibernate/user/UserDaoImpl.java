package br.com.jlabs.project.dao.hibernate.user;

import org.springframework.stereotype.Component;

import br.com.jlabs.project.dao.AbstractHibernateDao;
import br.com.jlabs.project.dao.user.UserDAO;
import br.com.jlabs.project.entity.User;

@Component("userDAO")
public class UserDaoImpl extends AbstractHibernateDao<User> implements UserDAO {

	public UserDaoImpl() {
		super(User.class);
	}
//	
//	public User save(User user) {
//		Session session;
//		
//		try {
//			session = sessionFactory.getCurrentSession();
//			return (User) session.merge(user);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	public List<User> list() {
//		Session session;
//		
//		try {
//			session = sessionFactory.getCurrentSession();
//			Criteria criteria = session.createCriteria(User.class);
//			return criteria.list();
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	public User find(Integer id) {
//		Session session;
//		
//		try {
//			session = sessionFactory.getCurrentSession();
//			Criteria criteria = session.createCriteria(User.class);
//			criteria.add(Restrictions.eq("id", id));
//			return (User) criteria.uniqueResult();
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//	}

}
