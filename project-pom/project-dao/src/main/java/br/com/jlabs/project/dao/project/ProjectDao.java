package br.com.jlabs.project.dao.project;

import java.io.Serializable;
import java.util.List;

import br.com.jlabs.project.entity.Company;
import br.com.jlabs.project.entity.Project;

public interface ProjectDao {

	List<Project> list(Company company);

	Project getProjectById(Serializable id);

	Project save(Project project);
	
}
