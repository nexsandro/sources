package br.com.jlabs.project.dao.discipline;

import br.com.jlabs.project.dao.IGenericDao;
import br.com.jlabs.project.entity.Discipline;

public interface DisciplineDao extends IGenericDao<Discipline> {

	Discipline save(Discipline discipline);

	Discipline getDisciplineById(Integer id);

}
