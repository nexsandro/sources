package br.com.jlabs.project.dao.hibernate.project;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.jlabs.project.dao.project.ProjectDao;
import br.com.jlabs.project.entity.Company;
import br.com.jlabs.project.entity.Project;

@Component("projectDao")
public class ProjectDaoImpl implements ProjectDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Project> list(Company company) {
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session
				.createCriteria(Project.class)
				.createAlias("company", "company")
				.add(Restrictions.eq("company.id", company.getId()));
		
		return crit.list();
	}
	
	public Project getProjectById(Serializable id) {

		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session
				.createCriteria(Project.class)
				.add(Restrictions.eq("id", id));
		
		return (Project) crit.uniqueResult();
	}
	
	public Project save(Project project) {
		Session session = sessionFactory.getCurrentSession();
		
 		project.setCompany((Company) session.load(Company.class, project.getCompany().getId()));
		session.save(project);
		return project;
	}

	/**
	 * @param sessionFactory the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
