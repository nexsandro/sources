package br.com.jlabs.project.dao.hibernate.company;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Repository;

import br.com.jlabs.project.dao.AbstractHibernateDao;
import br.com.jlabs.project.dao.company.CompanyDao;
import br.com.jlabs.project.entity.Company;

@Repository("companyDao")
public class CompanyDaoImpl extends AbstractHibernateDao<Company> implements CompanyDao {

	public CompanyDaoImpl() {
		super(Company.class);
	}
	
	public List<Company> list() {
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Company.class);
		
		return crit.list();
	}

	public void save(Company company) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(company.getAddress());
		session.saveOrUpdate(company);
	}

//	public Company getCompanyById(Integer id) {
//		Session session = sessionFactory.getCurrentSession();
//		
//		Company company = (Company) session.createCriteria(Company.class)
//			.add(Restrictions.eq("id", id))
//			.createAlias("address", "address")
//			.uniqueResult();
//		
//		return company;
//	}

}
