package br.com.jlabs.project.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractHibernateDao<T extends Serializable> {

	private Class<T> clazz;

	/**
	 * Default constructor
	 */
	public AbstractHibernateDao(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Autowired
	protected SessionFactory sessionFactory;

	public final void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T findOne(long id) {
		return (T) getCurrentSession().get(clazz, id);
	}
	
	public T findOne(String idField, Serializable idValue, String...joinFetchs) {
		Criteria criteria = createJoinedCriteria(joinFetchs);
		
		criteria.add(Restrictions.eq(idField, idValue));
		
		return (T) criteria.uniqueResult();
	}


	public List<T> findAll() {
		return getCurrentSession().createQuery("from " + clazz.getName())
				.list();
	}
	
	public List<T> findAll(String ... joinFetchs) {
		Criteria criteria = createJoinedCriteria(joinFetchs);
		
		return criteria.list();
	}

	private Criteria createJoinedCriteria(String... joinFetchs) {
		Session currentSession = getCurrentSession();
		Criteria criteria = currentSession.createCriteria(clazz);
		
		for(String joinFetch: joinFetchs) {
			criteria.setFetchMode(joinFetch, FetchMode.JOIN);
		}
		return criteria;
	}
	
	public void create(T entity) {
		getCurrentSession().persist(entity);
	}

	public T update(T entity) {
		return (T) getCurrentSession().merge(entity);
	}

	public void delete(T entity) {
		getCurrentSession().delete(entity);
	}

	public void deleteById(long entityId) {
		T entity = findOne(entityId);
		delete(entity);
	}

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}