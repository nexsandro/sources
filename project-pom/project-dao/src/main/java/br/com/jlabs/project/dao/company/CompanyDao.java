package br.com.jlabs.project.dao.company;

import java.util.List;

import br.com.jlabs.project.dao.IGenericDao;
import br.com.jlabs.project.entity.Company;

public interface CompanyDao extends IGenericDao<Company> {

	List<Company> list();

	void save(Company company);

}
