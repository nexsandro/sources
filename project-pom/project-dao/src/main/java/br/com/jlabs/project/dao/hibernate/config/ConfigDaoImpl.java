package br.com.jlabs.project.dao.hibernate.config;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import br.com.jlabs.project.dao.AbstractHibernateDao;
import br.com.jlabs.project.dao.config.ConfigDao;
import br.com.jlabs.project.entity.ConfigParamEntity;
import br.com.jlabs.project.entity.enumeration.ConfigParam;

@Repository("configDao")
public class ConfigDaoImpl extends AbstractHibernateDao<ConfigParamEntity> implements ConfigDao {

	public ConfigDaoImpl() {
		super(ConfigParamEntity.class);
	}
	
	public ConfigParamEntity getParam(ConfigParam param) {
		Session session;
		
		try {
			session = sessionFactory.getCurrentSession();
			ConfigParamEntity configParam = (ConfigParamEntity) session.get(ConfigParamEntity.class, param); 
			
			return configParam;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
