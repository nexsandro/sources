package br.com.jlabs.project.dao.file;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import br.com.jlabs.project.dao.IGenericDao;
import br.com.jlabs.project.entity.Dir;
import br.com.jlabs.project.entity.File;
import br.com.jlabs.project.entity.FileHistory;

public interface FileDao extends IGenericDao<File> {
	
	Dir createDir(Dir parent, String name);

	Dir getDirById(Serializable id);

	File getFileById(Serializable id);

	Dir saveDir(Dir dir);

	File saveFile(File file);

	FileHistory saveHistory(FileHistory fileHistory);

	Map<String, List<Object>> listDir(Dir dir);

	Dir dirWithParents(Serializable dirId);

	Dir getParentDirById(Serializable childId);

	List<FileHistory> getHistory(File file);

	boolean existFileNameInDir(String fileName, Long dirId);
	
}
