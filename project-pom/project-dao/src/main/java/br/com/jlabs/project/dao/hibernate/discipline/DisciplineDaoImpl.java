package br.com.jlabs.project.dao.hibernate.discipline;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import br.com.jlabs.project.dao.AbstractHibernateDao;
import br.com.jlabs.project.dao.discipline.DisciplineDao;
import br.com.jlabs.project.entity.Discipline;

@Component("disciplineDao")
public class DisciplineDaoImpl extends AbstractHibernateDao<Discipline> implements DisciplineDao {

	public DisciplineDaoImpl() {
		super(Discipline.class);
	}

	public Discipline save(Discipline discipline) {
		Session session;
		
		try {
			session = sessionFactory.getCurrentSession();
			session.saveOrUpdate(discipline);
			return discipline;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Discipline getDisciplineById(Integer id) {
		Session session;
		
		try {
			session = sessionFactory.getCurrentSession();
			
			return (Discipline) session.createCriteria(Discipline.class)
					.add(Restrictions.eq("id", id))
					.uniqueResult();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
