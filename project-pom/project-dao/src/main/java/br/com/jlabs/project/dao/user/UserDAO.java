package br.com.jlabs.project.dao.user;

import br.com.jlabs.project.dao.IGenericDao;
import br.com.jlabs.project.entity.User;

public interface UserDAO extends IGenericDao<User> {

}